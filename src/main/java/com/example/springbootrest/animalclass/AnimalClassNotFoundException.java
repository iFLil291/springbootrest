package com.example.springbootrest.animalclass;

public class AnimalClassNotFoundException extends Exception {
    public AnimalClassNotFoundException(String id) {
        super(id);
    }
}
