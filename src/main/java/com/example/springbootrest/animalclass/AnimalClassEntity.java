package com.example.springbootrest.animalclass;

import com.example.springbootrest.animal.AnimalEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "animal_class")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnimalClassEntity {
    @Id
    @Column(name = "id", columnDefinition = "varchar(36)")
    String id;

    String name;

    @OneToMany(mappedBy = "animalClass")
    Set<AnimalEntity> animal;

    public AnimalClassEntity(String name) {

    }
}
