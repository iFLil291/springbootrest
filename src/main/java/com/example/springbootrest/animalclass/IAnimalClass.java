package com.example.springbootrest.animalclass;

import java.util.Set;

public interface IAnimalClass {
    String getId();

    String getName();
    void setName(String name);

    Set<String> getAnimal();
    void setAnimal(Set<String> animal);
}
