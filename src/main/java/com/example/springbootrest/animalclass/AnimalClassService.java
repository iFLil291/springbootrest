package com.example.springbootrest.animalclass;


import com.example.springbootrest.animal.AnimalEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class AnimalClassService {

    AnimalClassRepository animalClassRepository;

    public AnimalClassEntity get(String id) throws AnimalClassNotFoundException {
        return animalClassRepository.findAnimalClassEntitiesById(id)
                .orElseThrow(() -> new AnimalClassNotFoundException(id));
    }

    public AnimalClassEntity createAnimalClass(String name) {
        AnimalClassEntity animalClassEntity = new AnimalClassEntity(
                UUID.randomUUID().toString(),
                name,
                null
        );
        return animalClassRepository.save(animalClassEntity);
    }

    public Stream<AnimalClassEntity> all() {
        return animalClassRepository.findAll().stream();
    }
}
