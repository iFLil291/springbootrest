package com.example.springbootrest.animalclass;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnimalClassRepository extends JpaRepository<AnimalClassEntity, String> {
    Optional<AnimalClassEntity> findAnimalClassEntitiesById(String id);
    String createAnimalClass(String name);
}
