package com.example.springbootrest.animalclass;

import com.example.springbootrest.animal.AnimalEntity;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/animal_class")
@AllArgsConstructor
public class AnimalClassController {

    AnimalClassService animalClassService;

    @GetMapping
    public ResponseEntity<List<AnimalClassController.AnimalClassDTO>> all() {
        return ResponseEntity.ok(animalClassService.all()
                .map(AnimalClassController::toDTO)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnimalClassDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(animalClassService.get(id)));
        } catch (
                AnimalClassNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<AnimalClassDTO> createAnimalClass(@RequestBody CreateAnimalClassDTO createAnimalClassDTO) {
        return ResponseEntity.ok(toDTO(animalClassService.createAnimalClass(
                createAnimalClassDTO.getName())
        ));
    }

    private static AnimalClassDTO toDTO(AnimalClassEntity animalClassEntity) {
        return new AnimalClassDTO(
                animalClassEntity.getId(),
                animalClassEntity.getName()
        );
    }

    @Value
    public static class AnimalClassDTO {
        String id;
        String name;
    }

    @Value
    public static class CreateAnimalClassDTO {
        String name;
    }

    @Value
    public static class UpdateAnimalClassDTO {
        String name;
    }
}
