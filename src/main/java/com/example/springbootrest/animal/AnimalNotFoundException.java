package com.example.springbootrest.animal;

public class AnimalNotFoundException extends Exception {
    public AnimalNotFoundException(String id) {
        super(id);
    }
}
