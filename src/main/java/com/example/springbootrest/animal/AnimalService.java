package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassEntity;
import com.example.springbootrest.animalclass.AnimalClassNotFoundException;
import com.example.springbootrest.animalclass.AnimalClassRepository;
import com.example.springbootrest.remoteAPI.JsonPlaceholderRemote;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class AnimalService {

    AnimalRepository animalRepository;
    JsonPlaceholderRemote jsonPlaceholderRemote;
    AnimalClassRepository animalClassRepository;

    public Stream<AnimalEntity> all() {
        return animalRepository.findAll().stream();
    }

    public AnimalEntity createAnimal(String name, String binomialName) {
        AnimalEntity animalEntity = new AnimalEntity(
                UUID.randomUUID().toString(),
                name,
                binomialName,
                "",
                "",
                null
        );
        return animalRepository.save(animalEntity);
    }

    public AnimalEntity get(String id) throws AnimalNotFoundException {
        return animalRepository.findAnimalEntityById(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
    }

    public AnimalEntity updateAnimal(String id, String name, String binomialName) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        animalEntity.setName(name);
        animalEntity.setBinomialName(binomialName);
        return animalRepository.save(animalEntity);
    }

    public void delete(String id) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        animalRepository.delete(animalEntity);
    }

    public AnimalEntity link(String id, String remoteId) throws AnimalNotFoundException {
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));
        JsonPlaceholderRemote.JsonPlaceHolder json = jsonPlaceholderRemote.get(remoteId);
        animalEntity.setDescription(json.getBody());
        return animalRepository.save(animalEntity);
    }

    public AnimalEntity linkAnimalClass(String id, String animalClassId) throws AnimalNotFoundException, AnimalClassNotFoundException {
        AnimalEntity animalEntity = animalRepository.findAnimalEntityById(id)
                .orElseThrow(() -> new AnimalNotFoundException(id));

        AnimalClassEntity animalClassEntity = animalClassRepository.findAnimalClassEntitiesById(animalClassId)
                .orElseThrow(() -> new AnimalClassNotFoundException(animalClassId));

        animalEntity.setAnimalClass(animalClassEntity);
        return animalRepository.save(animalEntity);
    }
}
