package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassEntity;

public interface IAnimal {
    String getId();

    String getName();
    void setName(String name);

    String getBinomialName();
    void setBinomialName(String binomialName);

    String getDescription();
    void setDescription(String description);

    String getConservationStatus();
    void setConservationStatus(String conservationStatus);

    String getAnimalClass();
    void linkAnimalClass(String animalClassId);
}
