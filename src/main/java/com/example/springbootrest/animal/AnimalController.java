package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassNotFoundException;
import com.example.springbootrest.animalclass.AnimalClassRepository;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/animals")
@AllArgsConstructor
public class AnimalController {

    AnimalService animalService;
    AnimalClassRepository animalClassRepository;

    @GetMapping
    public ResponseEntity<List<AnimalDTO>> all() {
        return ResponseEntity.ok(animalService.all()
                .map(AnimalController::toDTO)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<AnimalDTO> createAnimal(@RequestBody CreateAnimalDTO createAnimalDTO) {
        return ResponseEntity.ok(toDTO(animalService.createAnimal(
                createAnimalDTO.getName(),
                createAnimalDTO.getBinomialName())
        ));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnimalDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(animalService.get(id)));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}/link/{remoteId}")
    public ResponseEntity<AnimalDTO> link(@PathVariable("id") String id, @PathVariable("remoteId") String remoteId) {
        try {
            return ResponseEntity.ok(toDTO(animalService.link(id, remoteId)));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}/linkAnimalClass/{animalClassId}")
    public ResponseEntity<AnimalDTO> linkAnimalClass(@PathVariable("id") String id, @PathVariable("animalClassId") String animalClassId) {
        try {
            return ResponseEntity.ok(toDTO(animalService.linkAnimalClass(id, animalClassId)));
        } catch (AnimalNotFoundException | AnimalClassNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<AnimalDTO> update(@PathVariable("id") String id, @RequestBody UpdateAnimalDTO updateAnimalDTO) {
        try {
            return ResponseEntity.ok(toDTO(
                    animalService.updateAnimal(
                            id,
                            updateAnimalDTO.getName(),
                            updateAnimalDTO.getBinomialName()
                    )
            ));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            animalService.delete(id);
            return ResponseEntity.ok().build();
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private static AnimalDTO toDTO(AnimalEntity animalEntity) {
        return new AnimalDTO(
                animalEntity.getId(),
                animalEntity.getName(),
                animalEntity.getBinomialName(),
                animalEntity.getDescription(),
                animalEntity.getConservationStatus(),
                animalEntity.getAnimalClass() != null ? animalEntity.getAnimalClass().getName() : ""
        );
    }

    @Value
    public static class AnimalDTO {
        String id;
        String name;
        String binomialName;
        String description;
        String conservationStatus;
        String animalClass;
    }

    @Value
    public static class CreateAnimalDTO {
        String name;
        String binomialName;
    }

    @Value
    public static class UpdateAnimalDTO {
        String name;
        String binomialName;
    }
}
