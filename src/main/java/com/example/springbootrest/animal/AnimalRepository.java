package com.example.springbootrest.animal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalEntity, String> {
    Optional<AnimalEntity> findAnimalEntityById(String id);
}
