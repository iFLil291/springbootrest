package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "animal")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnimalEntity {
    @Id
    @Column(name = "id", columnDefinition = "varchar(36)")
    String id;

    String name;

    String binomialName;

    String description;

    String conservationStatus;

    @ManyToOne()
    @JoinColumn(name = "animal_class_id")
    AnimalClassEntity animalClass;

}
