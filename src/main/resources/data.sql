INSERT INTO animal_class (id, name) VALUES ('1', 'Amphibian');
INSERT INTO animal_class (id, name) VALUES ('2', 'Bird');
INSERT INTO animal_class (id, name) VALUES ('3', 'Fish');
INSERT INTO animal_class (id, name) VALUES ('4', 'Invertebrate');
INSERT INTO animal_class (id, name) VALUES ('5', 'Mammal');
INSERT INTO animal_class (id, name) VALUES ('6', 'Reptile');