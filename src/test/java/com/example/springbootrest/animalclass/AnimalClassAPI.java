package com.example.springbootrest.animalclass;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.channels.MembershipKey;
import java.util.Set;

@AllArgsConstructor
public class AnimalClassAPI {
    private static final String BASE_URL = "/api/animal_class/";

    WebTestClient webTestClient;

    public Flux<AnimalClassDTO> all() {
        return webTestClient.get().uri(BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalClassDTO.class)
                .getResponseBody();
    }

    public Mono<AnimalClassDTO> get(String id) {
        return webTestClient.get().uri(BASE_URL + id)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalClassDTO.class)
                .getResponseBody()
                .single();
    }

    public AnimalClassDTO createAnimalClass(String name) {
        return webTestClient.post().uri(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CreateAnimalClassDTO(name))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalClassDTO.class)
                .getResponseBody()
                .blockLast();
    }

    @Value
    static class AnimalClassDTO {
        String id;
        String name;
        Set<String> animal;

        @JsonCreator
        public AnimalClassDTO(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name,
                @JsonProperty("animal") Set<String> animal
        ) {
            this.id = id;
            this.name = name;
            this.animal = animal;
        }
    }
    @Value
    static class CreateAnimalClassDTO {
        String name;
    }
}
