package com.example.springbootrest.animalclass;

import com.example.springbootrest.animal.AnimalAPI;
import com.example.springbootrest.animal.AnimalEntity;
import com.example.springbootrest.remoteAPI.JsonPlaceholderRemote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AnimalClassTest {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    AnimalClassRepository animalClassRepository;

    @MockBean
    JsonPlaceholderRemote jsonPlaceholderRemote;    // GroupRemote

    AnimalClassAPI animalClassAPI;

    @BeforeEach
    void setUp() {
        animalClassAPI = new AnimalClassAPI(webTestClient);
    }

    @Test
    void test_get_animal_classes_success() {
        // Given
        AnimalClassEntity animalClassEntity = mock(AnimalClassEntity.class);
        when(animalClassEntity.getName()).thenReturn("Mammal");
        when(animalClassRepository.findAll()).thenReturn(List.of(animalClassEntity));

        // When
        List<AnimalClassAPI.AnimalClassDTO> animalClasses = animalClassAPI.all()
                .collectList()
                .block();

        // Then
        assertEquals(1, animalClasses.size());
        assertEquals("Mammal",
                animalClasses.get(0).getName());
    }

    @Test
    void test_get_animal_class_success() {
        // Given
        AnimalClassEntity animalClassEntity = mock(AnimalClassEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalClassRepository.findAnimalClassEntitiesById(eq(id))).thenReturn(Optional.of(animalClassEntity));
        when(animalClassEntity.getId()).thenReturn(id);

        // When
        AnimalClassAPI.AnimalClassDTO animal = animalClassAPI.get(id)
                .block();

        // Then
        assertEquals(id, animal.getId());
        verify(animalClassRepository, times(1)).findAnimalClassEntitiesById(id);
    }

    @Test
    void test_create_animal_class_success() {
        // Given
        AnimalClassEntity animalClassEntity = mock(AnimalClassEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalClassRepository.save(any(AnimalClassEntity.class))).thenReturn(animalClassEntity);
        when(animalClassEntity.getId()).thenReturn(id);
        when(animalClassEntity.getName()).thenReturn("Mammal");

        // When
        AnimalClassAPI.AnimalClassDTO animalClass = animalClassAPI.createAnimalClass("Mammal");

        // Then
        verify(animalClassRepository, times(1)).save(any(AnimalClassEntity.class));
        assertEquals("Mammal", animalClass.getName());
    }
}
