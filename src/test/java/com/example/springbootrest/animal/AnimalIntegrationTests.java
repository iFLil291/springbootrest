package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassEntity;
import com.example.springbootrest.animalclass.AnimalClassRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AnimalIntegrationTests {
    @MockBean
    AnimalClassRepository mockAnimalClassRepository;

    @Autowired
    ApplicationContext context;

    @Autowired
    WebTestClient webTestClient;

    AnimalAPI animalAPI;

    @BeforeEach
    public void setUp() {
        animalAPI = new AnimalAPI(webTestClient);
    }

    @Test
    void test_get_all_animal_classes_success(){

    }

    @Test
    public void test_get_animal_class_by_id_success() {
        // Given
        Mockito.when(mockAnimalClassRepository.count()).thenReturn(123L);

        // When
        AnimalClassRepository animalClassRepositoryContext = context.getBean(AnimalClassRepository.class);
        long animalClassCount = animalClassRepositoryContext.count();

        // Then
        assertEquals(123L, animalClassCount);
        Mockito.verify(mockAnimalClassRepository).count();
    }

    @Test
    void test_add_animal_class_to_animal_success() {
        // Given
        AnimalClassEntity animalClassEntity = mock(AnimalClassEntity.class);
        String id = UUID.randomUUID().toString();
        AnimalAPI.AnimalDTO animal = animalAPI.createAnimal("Elk", "Cervus elaphus");

        // When
        animal = animalAPI.linkAnimalClass(animal.getId(), id);

        // Then
        assertEquals("Mammal", animal.getAnimalClassFromId());
    }
}
