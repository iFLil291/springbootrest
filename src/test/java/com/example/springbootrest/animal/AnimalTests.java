package com.example.springbootrest.animal;

import com.example.springbootrest.animalclass.AnimalClassEntity;
import com.example.springbootrest.animalclass.AnimalClassRepository;
import com.example.springbootrest.remoteAPI.JsonPlaceholderRemote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AnimalTests {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    AnimalRepository animalRepository;

    @MockBean
    AnimalClassRepository animalClassRepository;    // GroupRemote

    @MockBean
    JsonPlaceholderRemote jsonPlaceholderRemote;    // GroupRemote

    AnimalAPI animalAPI;

    @BeforeEach
    void setUp() {
        animalAPI = new AnimalAPI(webTestClient);
    }

    @Test
    void test_get_animals_success() {
        // Given
        AnimalEntity animalEntity = mock(AnimalEntity.class);
        when(animalEntity.getName()).thenReturn("Elk");
        String binomialName = "Cervus elaphus";
        when(animalEntity.getBinomialName()).thenReturn(binomialName);
        when(animalRepository.findAll()).thenReturn(List.of(animalEntity));

        // When
        List<AnimalAPI.AnimalDTO> animals = animalAPI.all()
                .collectList()
                .block();

        // Then
        assertEquals(1, animals.size());
        assertEquals("Elk", animals.get(0).getName());
    }

    @Test
    void test_get_animal_success() {
        // Given
        AnimalEntity animalEntity = mock(AnimalEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalRepository.findAnimalEntityById(eq(id))).thenReturn(Optional.of(animalEntity));
        when(animalEntity.getId()).thenReturn(id);

        // When
        AnimalAPI.AnimalDTO animal = animalAPI.get(id)
                .block();

        // Then
        assertEquals(id, animal.getId());
        verify(animalRepository, times(1)).findAnimalEntityById(id);
    }

    @Test
    void test_create_animal_success() {
        // Given
        AnimalEntity animalEntity = mock(AnimalEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalRepository.save(any(AnimalEntity.class))).thenReturn(animalEntity);
        when(animalEntity.getId()).thenReturn(id);
        when(animalEntity.getName()).thenReturn("Elk");
        when(animalEntity.getBinomialName()).thenReturn("Cervus elaphus");

        // When
        AnimalAPI.AnimalDTO animal = animalAPI.createAnimal("Elk", "Cervus elaphus");

        // Then
        verify(animalRepository, times(1)).save(any(AnimalEntity.class));
        assertEquals("Elk", animal.getName());
        assertEquals("Cervus elaphus", animal.getBinomialName());
    }

    @Test
    void test_update_animal_success() {
        // Given
        AnimalEntity oldAnimal = mock(AnimalEntity.class);
        AnimalEntity newAnimal = mock(AnimalEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalRepository.findAnimalEntityById(id)).thenReturn(Optional.of(oldAnimal));
        when(animalRepository.save(eq(oldAnimal))).thenReturn(newAnimal);
        when(newAnimal.getName()).thenReturn("Parrot");

        // When
        AnimalAPI.AnimalDTO updatedAnimal = animalAPI.updateAnimal(id, "Parrot", "Aprosmictus erythropterus");

        // Then
        assertEquals("Parrot", updatedAnimal.getName());
        verify(animalRepository, times(1)).save(oldAnimal);
        verify(oldAnimal, times(1)).setName("Parrot");
        verify(animalRepository, times(1)).findAnimalEntityById(id);
    }

    @Test
    void test_delete_animal_success() {
        // Given
        AnimalEntity animalEntity = mock(AnimalEntity.class);
        String id = UUID.randomUUID().toString();
        when(animalRepository.findAnimalEntityById(id)).thenReturn(Optional.of(animalEntity));

        // When
        animalAPI.delete(id)
                .block();

        // Then
        verify(animalRepository, times(1)).delete(eq(animalEntity));
    }

    @Test
    void test_add_description_to_animal_success() {
        // Given

        // When

        // Then
    }

    @Test
    void test_remove_description_from_animal_success() {
        // Given

        // When

        // Then
    }

    @Test
    void test_add_animal_class_to_animal_success() {
        // Given
        AnimalClassEntity animalClassEntity = mock(AnimalClassEntity.class);
        String animalClassId = UUID.randomUUID().toString();
        String id = UUID.randomUUID().toString();
        AnimalEntity oldAnimal = mock(AnimalEntity.class);
        AnimalEntity newAnimal = mock(AnimalEntity.class);
        when(newAnimal.getAnimalClass()).thenReturn(animalClassEntity);
        when(animalRepository.findAnimalEntityById(eq(id))).thenReturn(Optional.of(oldAnimal));
        when(animalRepository.save(eq(oldAnimal))).thenReturn(newAnimal);
        when(animalClassRepository.createAnimalClass(eq("Mammal"))).thenReturn(animalClassId);
        when(animalClassRepository.findAnimalClassEntitiesById(animalClassId)).thenReturn(Optional.of(animalClassEntity));

        // When
        AnimalAPI.AnimalDTO animalWithAddedAnimalClass = animalAPI.linkAnimalClass(id, animalClassId);

        // Then
        assertEquals("Mammal", animalWithAddedAnimalClass.getAnimalClassFromId());
        verify(animalClassRepository, times(1)).createAnimalClass(animalClassId);
        verify(oldAnimal, times(1)).setAnimalClass(eq(animalClassEntity));
    }

    @Test
    void test_remove_animal_class_from_animal_success() {
        // Given

        // When

        // Then
    }
}
