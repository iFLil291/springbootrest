package com.example.springbootrest.animal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class AnimalAPI {
    private static final String BASE_URL = "/api/animals/";

    WebTestClient webTestClient;

    public Flux<AnimalDTO> all() {
        return webTestClient.get().uri(BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalDTO.class)
                .getResponseBody();
    }

    public Mono<AnimalDTO> get(String id) {
        return webTestClient.get().uri(BASE_URL + id)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalDTO.class)
                .getResponseBody()
                .single();
    }

    public AnimalDTO createAnimal(String name, String binomialName) {
        return webTestClient.post().uri(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CreateAnimalDTO(name, binomialName))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalDTO.class)
                .getResponseBody()
                .blockLast();
    }

    public AnimalDTO updateAnimal(String id, String name, String binomialName) {
        return webTestClient.put().uri(BASE_URL + id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new UpdateAnimalDTO(name, binomialName))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalDTO.class)
                .getResponseBody()
                .blockLast();
    }

    public Mono<Void> delete(String id) {
        return webTestClient.delete().uri(BASE_URL + id)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Void.class)
                .getResponseBody()
                .then();
    }

    public AnimalDTO linkAnimalClass(String id, String animalClassId) {
        return webTestClient.put().uri(BASE_URL + id + "/linkAnimalClass/" + animalClassId)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(AnimalDTO.class)
                .getResponseBody()
                .blockLast();
    }

    @Value
    static class AnimalDTO {
        String id;
        String name;
        String binomialName;
        String description;
        String conservationStatus;
        String animalClassFromId;

        @JsonCreator
        public AnimalDTO(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name,
                @JsonProperty("binomialName") String binomialName,
                @JsonProperty("description") String description,
                @JsonProperty("conservationStatus") String conservationStatus,
                @JsonProperty("animalClass") String animalClassFromId) {
                    this.id = id;
                    this.name = name;
                    this.binomialName = binomialName;
                    this.description = description;
                    this.conservationStatus = conservationStatus;
                    this.animalClassFromId = animalClassFromId;
        }
    }

    @Value
    static class CreateAnimalDTO {
        String name;
        String binomialName;
    }

    @Value
    static class UpdateAnimalDTO {
        String name;
        String binomialName;
    }
}
